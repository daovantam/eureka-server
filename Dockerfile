FROM openjdk:8
ADD target/eureka-server.jar eureka-server.jar
EXPOSE 8761
EntryPoint ["java", "-jar", "eureka-server.jar"]